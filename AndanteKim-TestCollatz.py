#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "20 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 20)
        self.assertEqual(j, 20)

    def test_read_3(self):
        s = "675 475\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 675)
        self.assertEqual(j, 475)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        # modify from 1 to 20
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        # modify from 1 to 125
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        # modify from 1 to 89
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        # modify from 1 to 174
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # new add 5 tests
    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(100, 20)
        self.assertEqual(v, 119)

    def test_eval_7(self):
        v = collatz_eval(1502, 1209)
        self.assertEqual(v, 177)

    def test_eval_8(self):
        v = collatz_eval(261771, 260000)
        self.assertEqual(v, 332)

    def test_eval_9(self):
        v = collatz_eval(555000, 555555)
        self.assertEqual(v, 390)

    def test_eval_10(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 200, 20, 125)
        self.assertEqual(w.getvalue(), "200 20 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 500, 500, 111)
        self.assertEqual(w.getvalue(), "500 500 111\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 999999, 999995, 259)
        self.assertEqual(w.getvalue(), "999999 999995 259\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("200 100\n1 1\n999999 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "200 100 125\n1 1 1\n999999 999999 259\n")

    def test_solve_3(self):
        r = StringIO("1000 500\n999 954\n2000 2000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 500 179\n999 954 143\n2000 2000 113\n")

    def test_solve_4(self):
        r = StringIO("9999 9000\n1000 1999\n36666 30000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9999 9000 260\n1000 1999 182\n36666 30000 324\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""

